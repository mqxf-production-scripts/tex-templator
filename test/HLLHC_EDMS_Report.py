import pandas as pd
import sys
sys.path.insert(0, "../")

from textemplator import TexTemplator

def test_report():
    report = TexTemplator('test.tex', 'hllhcedms_template.tex')
    doc_data = {}
    doc_data['%EDMS_NUMBER'] = '1234'
    doc_data['%REV'] = '1234'
    doc_data['%VALIDITY'] = 'DRAFT'
    doc_data['%REFERENCE'] = 'XXX-EQCODE-XX-XXX'
    doc_data['%PREPARED_BY'] = 'N. Surname [Author/s]'
    doc_data['%PREPARED_DATE'] = '20YY-MM-DD'
    doc_data['%VERIFIED_BY'] = 'N. Surname [People with relevant experience in the field]'
    doc_data['%VERIFIED_DATE'] = '20YY-MM-DD'
    doc_data['%APPROVED_BY'] = 'N. Surname [Project hierarchy E.g. WP Leader, PL, ...]'
    doc_data['%APPROVED_DATE'] = '20YY-MM-DD'
    doc_data['%FIG1'] = report.figure('CERNLogo.eps', 'fig:logo', caption='test figure')
    doc_data['%SOME_TEXT'] = 'This text wants to refer to figure \\ref{fig:logo}'

    data = {'A':[1,2,3,4], 'B':[2,3,4,5]}
    df = pd.DataFrame(data)

    doc_data['%TAB1'] = report.table(df.to_latex(), 'tab:data', caption='test table')
    report.write(doc_data)
    report.compile()

if __name__ == "__main__":
    test_report()


